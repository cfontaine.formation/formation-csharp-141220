﻿using System;

namespace _11___Exceptions
{
    // On peut créer ses propres exceptions en héritant de la classe Exception
    // Par convention, toutes les sous-classes ont un nom se terminant par Exception
    class AgeNegatifException : Exception
    {
        public int Age { get; private set; }

        public AgeNegatifException(int age) : base()    // base => appel au constructeur de la classe Exception
        {
            Age = age;
        }

        public AgeNegatifException(int age, string message) : base(message)
        {
            Age = age;
        }

        public AgeNegatifException(int age, string message, Exception innerException) : base(message, innerException)
        {
            Age = age;
        }
    }
}
