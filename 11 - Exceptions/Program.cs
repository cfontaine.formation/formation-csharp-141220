﻿using System;

namespace _11___Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tab = { 1, 2, 3, 4 };
            try
            {
                tab[1] = 3;                          // Génére une Exception IndexOutOfRangeException 
                int val = Convert.ToInt32("123");    // Génére une Exception FormatException 
                //   File.OpenRead("nexistepas");    // Génère  une Exception FileNotFoundException
                Console.WriteLine(val * 2);
            }
            catch (FormatException e)
            {
                Console.WriteLine("La chaine de caractère ne contient de nombre");
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);   // Message => permet de récupérer le messsage de l'exception
            }
            catch (Exception e) // Attrape tous les autreseException
            {
                Console.WriteLine("Toutes les autres exceptions");
                Console.WriteLine(e.Message);
            }
            finally  // Le bloc finally est toujours éxécuté
            {
                Console.WriteLine("Toujours executé");
            }
            try
            {
                GestionAge();
            }
            catch (Exception e)
            {
                Console.WriteLine("Main");
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);    // InnerException => exception qui est à l'origine de cette exception
                }
            }
            Console.ReadKey();
        }


        static void GestionAge()
        {
            try
            {
                int age = SaisirAge();
                Console.WriteLine(age);
            }   // Traitement partiel de l'exception AgeNegatifException 
            catch (AgeNegatifException e) when (e.Age > -45)   // when : le catch est éxécuté si la condition dans when est vrai
            {
                // traitement partial
                Console.WriteLine("Gestion Age inf -45");
                Console.WriteLine(e.Message);
                // throw ;   // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau
                throw new ArgumentOutOfRangeException(e.Message, e);      // On relance une autre exception ,e =>référence à l'exception interne qui a provoquer l'exception
            }
            catch (AgeNegatifException e) when (e.Age < -46)
            {
                // traitement partial
                Console.WriteLine("Gestion Age sup -46");
                Console.WriteLine(e.Message);
                // throw e;
                throw new ArgumentOutOfRangeException(e.Message, e);
            }
        }

        static int SaisirAge()
        {
            int age = Convert.ToInt32(Console.ReadLine());
            if (age < 0)
            {
                //throw new Exception(string.Format($"L'age ({age})est négatif"));
                throw new AgeNegatifException(age, "L'age est négatif");         //  throw => Lancer un exception
            }
            return age;
        }
    }
}
