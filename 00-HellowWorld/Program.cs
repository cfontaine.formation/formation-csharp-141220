﻿using System;

namespace _00_HellowWorld
{

    // Commentaire sur une ligne

    /*
     * Commentaire sur 
     * plusieurs lignes
     * */

    /// <summary>
    /// Ma première classe
    /// </summary>
    class Program
    {
        /// <summary>
        /// Méthode principale point d'entrée du programme
        /// </summary>
        /// <param name="args">Les arguments de la ligne de commande</param>
        static void Main()
        {
            Console.WriteLine("Hello world!");
            Console.ReadKey();       // Pour maintenir la console affichée

        }
    }
}
