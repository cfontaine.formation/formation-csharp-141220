﻿using System;
using System.IO;

namespace _13_Entree_sortie
{
    class Program
    {
        static void Main(string[] args)
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo d in drives)
            {
                Console.WriteLine(d.Name);              // Nom du lecteur
                Console.WriteLine(d.TotalSize);         // espace total du lecteur
                Console.WriteLine(d.TotalFreeSpace);    // espace disponible sur le lecteur
                Console.WriteLine(d.DriveFormat);       // système de fichiers du lecteur NTFS, FAT ..
            }
            //  Teste si le dossier existe
            if (!Directory.Exists(@"c:\Formations\TestIO\Csharp"))
            {
                Directory.CreateDirectory(@"c:\Formations\TestIO\Csharp");  // Création du répertoire
            }
            string[] paths = Directory.GetFiles(@"c:\Formations\TestIO\Csharp");    // liste les fichiers du répertoire
            foreach (var f in paths)
            {
                Console.WriteLine(f);
            }

            if (!File.Exists(@"c:\Formations\TestIO\Csharp\a.txt"))
            {
                File.CreateText(@"c:\Formations\TestIO\Csharp\a.txt");  // Crée le fichier a s'il n'existe pas
            }
            if (!File.Exists(@"c:\Formations\TestIO\Csharp\b.txt"))
            {
                File.CreateText(@"c:\Formations\TestIO\Csharp\b.txt");   // Crée le fichier a s'il n'existe pas
            }
            if (!File.Exists(@"c:\Formations\TestIO\Csharp\c.txt")) // si le fichier c.txt n'existe pas 
            {
                File.Move(@"c:\Formations\TestIO\Csharp\b.txt", @"c:\Formations\TestIO\Csharp\c.txt");  // On renomme le fichier b.txt en fichier c.txt
            }
            //EcrireFichierText(@"c:\Formations\TestIO\Csharp\test.txt");
            // LireFichierText(@"c:\Formations\TestIO\Csharp\test.txt");
            EcrireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");
            LireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");
            Console.ReadKey();
        }
        public static void LireFichierText(string path)
        {
            // Using => Équivalent d'un try / finally + Close()
            using (StreamReader sr = new StreamReader(path))    // StreamReader Lire un fichier texte
            {
                while (!sr.EndOfStream)  // Propriété EndOfStream est vrai si le fichier atteint la fin du fichier
                {
                    Console.WriteLine(sr.ReadLine());
                }
            }
        }

        public static void EcrireFichierText(string path)    // StreamWriter Ecrire un fichier texte
        {                                                    // append à true "compléte" le fichier s'il existe déjà, à false le fichier est écrasé
            using (StreamWriter sw = new StreamWriter(path, true))
            {
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello world");
                }
            }
            //sw.Close();
            //sw.Dispose();
        }

        public static void EcrireFichierBin(string path)    // FileStream =>  permet de Lire/Ecrire un fichier binaire
        {
            using (FileStream fs = new FileStream(path, FileMode.Append))
            {
                for (byte b = 0; b < 100; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBin(string path)
        {
            byte[] tab = new byte[10];
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                int nb = 1;
                while (nb != 0)
                {
                    nb = fs.Read(tab, 0, 10);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    foreach (var v in tab)      // Read => retourne le nombre d'octets lue dans le fichier                     {
                        Console.WriteLine(v);
                }
            }
        }
    }
}
