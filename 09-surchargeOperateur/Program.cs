﻿using System;

namespace _09_surchargeOperateur
{
    class Program
    {
        static void Main()
        {
            Point a = new Point(1, 1);
            Point b = new Point(2, 2);
            Point c = a + b;
            Console.WriteLine(c);
            Console.WriteLine(-c);
            Console.WriteLine(c * 3);
            Console.WriteLine(c * 3 == (a + b) * 3);
            Console.WriteLine(a == b);
            Console.WriteLine(a != b);
            a += b; // quand on surcharge un opérateur, l'opérateur composé associé est aussi automatiquement surchargé
            Console.WriteLine(a);

            // Exercice surcharge opérateur
            Fraction f1 = new Fraction(1, 4);
            Fraction f2 = new Fraction(1, 4);
            Fraction fr = f1 * f2;
            Console.WriteLine(fr);
            Console.WriteLine(f1 == f2);
            Console.WriteLine(f1 != f2);
            Console.WriteLine(f1 * 2);
            Console.ReadKey();
        }
    }
}
