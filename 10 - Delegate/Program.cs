﻿using System;

namespace _10___Delegate
{
    class Program
    {
        // Déléguation => Définition de prototypes de fonctions
        public delegate int Operation(int n1, int n2);
        public delegate bool Comparaison(int n1, int n2);

        // Méthodes correspondants au prototype Operation
        // Retourne un entier et a pour paramètre 2 entiers
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int n1, int n2)
        {
            return n1 * n2;
        }

        static void Main(string[] args)
        {
            // Appel d'un délégué C#
            Operation o = new Operation(Ajouter);
            AfficherCalcul(1, 2, o);
            AfficherCalcul(1, 2, new Operation(Multiplier));

            // Appel d'un délégué C# 2.0 => plus besoin de méthode correspondant au prototype
            Operation o2 = delegate (int n1, int n2) { return n1 + n2; };
            AfficherCalcul(1, 2, o2);
            AfficherCalcul(1, 2, delegate (int n1, int n2) { return n1 * n2; });

            // Appel d'un délégué C# 3.0 => utilisation des lambdas

            // Lambda =>méthode sans nom, les paramètres et le corps de la méthode sont séparés par l'opérateur =>
            // Syntaxe
            // () => expression     S'il n'y a pas de paramètre
            // 1 paramètre => expression
            // (paramètres) => expression
            // (paramètres) => { instructions }*
            AfficherCalcul(1, 2, (n1, n2) => n1 + n2);
            AfficherCalcul(1, 2, (n1, n2) => n1 * n2);


            // Excercice delegate
            int[] t = { 6, -5, 3, -1, 7, 2 };
            SortTab(t, (n1, n2) => n1 > n2);    // décroissant
            foreach (int v in t)
            {
                Console.WriteLine(v);
            }

            SortTab(t, (n1, n2) => n1 < n2);    // croissant
            foreach (int v in t)
            {
                Console.WriteLine(v);
            }
            Console.ReadKey();
        }

        // On utilise les délégués pour passer une méthode en paramètre à une autre méthode
        public static void AfficherCalcul(int a, int b, Operation op)
        {
            Console.WriteLine(op(a, b));
        }

        // Excercice delegate
        //- Ecrire le delegate Comparaison__ pour avoir un tri croissant ou décroissant
        //- Ecrire un lamba pour tri décroissant
        static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

    }
}
