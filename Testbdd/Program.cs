﻿using Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Testbdd
{

    // Le fournisseur de donnée mysql-data est à obtenir avec nuget
    // Pour ajouter le projet la bibliothèque Database au projet
    // Cliquer droit sur le projet -> Ajouter -> référence -> choisir Database 
    class Program
    {
        static void Main()
        {
            // La chaine de connection contient toutes les informations pour la connection à la base de donnée
            // Server-> adresse du serveur de bdd
            // Port port de serveur de bdd
            // Database -> nom de la base de donnée
            // Uid -> utilisateur de la base de donnée
            // Pwd -> mot de passe de la bdd
            //string chaineConnection = "Server=localhost;Port=3306;Database=formation;Uid=root;Pwd=dawan;";

            // Test de connexion à la base de donnée (MySql / Maria DB)
            // TestConnexion(chaineConnection);

            // Test du dao
            TestDao(ConfigurationManager.ConnectionStrings["chCnxMysql"].ConnectionString);

            Console.ReadKey();
        }

        private static void TestConnexion(string chaineConnection)
        {
            // création et ouverture de la connexion à la base de donnée MySqlConnection
            MySqlConnection cnx = new MySqlConnection(chaineConnection);
            cnx.Open();

            Contact c = SaisirContact();
            // Création de l'objet MySqlCommand qui contient et permet d'executer la requête
            string req1 = "INSERT INTO contacts(prenom,nom,jour_naissance,email)VALUES (@prenom,@nom,@dateNaissance,@email); ";
            MySqlCommand cmd = new MySqlCommand(req1, cnx);
            // Remplacement des paramètres dans la requête (@...) par les valeurs
            cmd.Parameters.AddWithValue("@prenom", c.Prenom);
            cmd.Parameters.AddWithValue("@nom", c.Nom);
            cmd.Parameters.AddWithValue("@dateNaissance", c.JourNaissance);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.ExecuteNonQuery();  // execution de la requete pour INSERT,UPDATE,DELETE
            c.Id = cmd.LastInsertedId; // Récupération de la clé primaire générée par la bdd

            // 
            string req = "Select id,prenom,nom,jour_naissance,email FROM contacts";
            cmd = new MySqlCommand(req, cnx);
            MySqlDataReader reader = cmd.ExecuteReader(); // execution de la requete pour SELECT, on récupère le résultat de la requête dans un objet MySqlDataReader
            while (reader.Read())   // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat
            {   // GetXXXX(string NomColonne) ou GetXXXX(string NumeroColonne) pour récupérer les valeurs
                Console.WriteLine($"{reader.GetString("prenom")}\t{reader.GetString("nom")}\t{reader.GetDateTime("jour_naissance")}\t{reader.GetString("email")}\t{reader.GetInt64("id")}");
            }
            cnx.Close(); // Fermeture de la connection
        }

        private static void TestDao(string chaineConnection)
        {
            //Récupération de la chaine de connection dans le fichier App.config du projet élément<connectionStrings>


            ContactDao.ChaineConnection = chaineConnection;
            ContactDao dao = new Database.ContactDao();
            Contact c = new Contact("John", "Doe", DateTime.Now, "jd@dawan.fr");
            Console.WriteLine("id={0}", c.Id); // id=0 => l'objet n'a pas été peristé dans la base de donnée
            dao.SaveOrUpdate(c, true); // persister l'objet dans la bdd
            Console.WriteLine("id={0}", c.Id); // id a été généré par la bdd
            Console.WriteLine("{0}", c);
            long id = c.Id;
            // Affichage de tous les objets contact
            List<Contact> lst = dao.FindAll(true);
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

            // Lire un objet à partir de son id
            Console.WriteLine("\nLire {0}", id);
            Contact cr = dao.FindById(id, true);
            Console.WriteLine(cr);

            // Modification 
            Console.WriteLine("\nModification {0}", cr.Id);
            cr.Prenom = "Marcel";
            cr.JourNaissance = new DateTime(1987, 8, 11);
            dao.SaveOrUpdate(cr, true);
            cr = dao.FindById(cr.Id, true);
            Console.WriteLine(cr);

            // Effacer
            Console.WriteLine("\neffacer {0}", cr.Id);
            dao.Delete(cr, true);
            lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }
        }

        private static Contact SaisirContact()
        {
            Console.Write("Entrer votre prénom: ");
            string prenom = Console.ReadLine();
            Console.Write("Entrer votre nom: ");
            string nom = Console.ReadLine();
            Console.Write("Entrer votre date de naissance (YYYY/MM/DD): ");
            DateTime jdn = DateTime.Parse(Console.ReadLine());
            Console.Write("Entrer votre email: ");
            string email = Console.ReadLine();
            return new Contact(prenom, nom, jdn, email);
        }
    }
}
