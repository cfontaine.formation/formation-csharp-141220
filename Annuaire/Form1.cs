﻿using Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Annuaire
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chCnxMysql"].ConnectionString;
            InitListView();
        }

        private void InitListView()
        {
            ContactDao dao = new ContactDao();
            List<Contact> lst = dao.FindAll();
            listView1.Items.Clear();
            foreach (Contact c in lst)
            {
                AddListView(c);
            }
        }

        private void AddListView(Contact contact)
        {
            ListViewItem item1 = new ListViewItem(contact.Prenom, 0);
            item1.SubItems.Add(contact.Nom);
            item1.SubItems.Add(contact.JourNaissance.ToShortDateString());
            item1.SubItems.Add(contact.Email);
            listView1.Items.Add(item1);
        }


        private void ClearTextBox()
        {
            textBoxEmail.Text = "";
            textBoxNom.Text = "";
            textPrenom.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ContactDao dao = new ContactDao();
            if (textPrenom.TextLength == 0 || textBoxNom.TextLength == 0 || textBoxEmail.TextLength == 0)
            {
                MessageBox.Show("Tous les champs ne sont pas remplis", "Ajouter un contact", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else if (!Regex.IsMatch(textBoxEmail.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"))
            {
                MessageBox.Show("L'email n'est pas au bon format", "Ajouter un contact", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (dao.IsEmailExist(textBoxEmail.Text))
            {
                MessageBox.Show("L'email existe déjà", "Ajouter un contact", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                Contact contact = new Contact(textPrenom.Text, textBoxNom.Text, dateTimeJnaissance.Value, textBoxEmail.Text);
                dao.SaveOrUpdate(contact);
                AddListView(contact);
                ClearTextBox();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var selected = listView1.SelectedItems;
            if (selected.Count == 0)
            {
                MessageBox.Show("Il n'a pas de ligne sélectionner", "Effacer un contact", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                ContactDao dao = new ContactDao();
                Contact c = dao.FindByEmail(selected[0].SubItems[3].Text);
                dao.Delete(c);
            }
            InitListView();
        }

    }
}

