﻿using System;

namespace _05_poo
{
    // Classe imbriquée
    public class Conteneur
    {
        private static string varClasse = "Variable de classe";

        private string varInstance = "Variable d'instance";

        public void TestConteneur()
        {
            Element elm = new Element();
            elm.TestElmVarClasse();
            elm.TestElmVarInstance(this);
        }

        class Element // par défaut private
        {
            public void TestElmVarClasse()
            {
                Console.WriteLine(varClasse);    // On a accès au variable de classe de extérieur
            }
            public void TestElmVarInstance(Conteneur c)
            {
                Console.WriteLine(c.varInstance);
            }
        }
    }
}
