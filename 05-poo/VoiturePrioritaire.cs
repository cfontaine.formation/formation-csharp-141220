﻿using System;

namespace _05_poo
{
    class VoiturePrioritaire : Voiture // VoiturePrioritaire hérite de Voiture
    {
        public VoiturePrioritaire(string marque, string couleur, string plaque, bool gyro) : base(marque, couleur, plaque) // base => pour appeler le constructeur de la classe mère
        {
            Gyro = gyro;
            Vitesse = 10;
        }
        public bool Gyro { get; set; }

        public new void Afficher()
        {
            base.Afficher();
            Console.WriteLine(Gyro ? "Gyro Allumé" : "Gyro éteint");
        }

        public override string ToString()
        {   // base => pour appeler une méthode de la classe mère
            return string.Format($" Voiture Priotaire [ gyro ={Gyro}  {base.ToString()}]");
        }
    }
}
