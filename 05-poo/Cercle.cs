﻿namespace _05_poo
{
    // Exercice Cercle
    // Écrire une classe Cercle
    // Propriété
    //  - centre : un point
    //  - rayon par défaut le rayon est égal à 1
    //
    // Constructeurs
    // Un constructeur
    //  - par défaut
    //  - qui permet d’initialiser le cercle
    //
    // Méthodes
    // Une méthode qui permet de tester
    //  - la collision entre 2 cercles
    //    s'il y a collision->La distance entre les centres doit inférieure ou égal à la somme des rayon
    //  - si un point est contenu dans le cercle
    // la distance entre le centre du cercle et le point <= au rayon du cercle
    class Cercle
    {
        public Point Centre { get; set; }
        public double Rayon { get; set; } = 1.0;

        public Cercle(Point centre, double rayon)
        {
            Centre = centre;
            Rayon = rayon;
        }

        static public bool Colision(Cercle c1, Cercle c2)
        {
            return Point.Distance(c1.Centre, c2.Centre) < c1.Rayon + c2.Rayon;
        }
        static public bool Contenu(Cercle c1, Point p)
        {
            return Point.Distance(c1.Centre, p) < c1.Rayon;
        }

    }
}
