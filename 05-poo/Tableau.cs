﻿namespace _05_poo
{
    class Tableau
    {
        int[] elm;
        public Tableau(int size)
        {
            elm = new int[size];
        }

        public Tableau(int size, int initVal)
        {
            elm = new int[size];
            for (int i = 0; i < elm.Length; i++)
            {
                elm[i] = initVal;
            }
        }

        public int this[int index]
        {
            get
            {
                return elm[index];
            }

            set
            {
                elm[index] = value;
            }
        }

        public void Add(int val)
        {
            int[] tmp = new int[elm.Length + 1];
            for (int i = 0; i < elm.Length; i++)
            {
                tmp[i] = elm[i];
            }
            tmp[elm.Length] = val;
            elm = tmp;
        }

        public int Lenght
        {
            get
            {
                return elm.Length;
            }
        }
    }
}
