﻿using System;
using System.Collections.Generic;

namespace _05_poo
{
    /*sealed*/
    class Voiture    //  sealed => pour empecher l'héritage de cette classe
    {
        // Variables d'instances => Etat

        // On n'a plus besoin de déclarer les variables d'instances, elles seront générées automatiquement par les propriétées auto-implémentés
        // private string Marque = "Opel";
        // private string Couleur;
        // private string PlaqueIma;
        // private int _vitesse;
        //  private int CompteurKM = 1000;

        // Variable de classe
        // public static int CompteurVoiture;      // Remplacer par une propriétée static

        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Marque { get; set; } = "Opel";    // On peut donner un valeur par défaut à une propriétée(littéral, expression ou une fonction)
        public string Couleur { get; set; }
        public string PlaqueIma { get; set; }
        public int Vitesse { get; protected set; }      // On peut associer un modificateur d'accès à get et à set. Il doit être plus restritif que le modificateur de la propriété 
        public int CompteurKM { get; } = 1000;
        public Personne Proprietaire { get; set; }

        //public int Vitesse
        //{
        //    get
        //    {
        //        return _vitesse;
        //    }
        //    set
        //    {
        //        if(value>0)
        //        {
        //            _vitesse = value;
        //        }
        //    }
        //}

        // C# 7.0
        //public string Marque
        //{
        //    get => marque;
        //    set => marque = value;
        //}

        public static int CompteurVoiture { get; private set; }

        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            CompteurVoiture++;
        }

        public Voiture(string marque, string couleur, string plaqueIma) : this()    // this() => Chainnage de constructeur : appel du constructeur par défaut
        {
            Marque = marque;
            Couleur = couleur;
            PlaqueIma = plaqueIma;
        }

        public Voiture(string marque, string couleur, string plaqueIma, int Vitesse, int compteurKM) : this(marque, couleur, plaqueIma)  // Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, string plaque)
        {
            this.Vitesse = Vitesse;
            CompteurKM = compteurKM;
        }

        // Exemple de constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur Statique");
        }

        // Destructeur => libérer des ressources 
        ~Voiture()
        {
            Console.WriteLine("Destructeur");
        }

        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                Vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                Vitesse -= vFrn;
            }
            if (Vitesse < 0)
            {
                Vitesse = 0;
            }
        }

        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public void Afficher()
        {
            Console.WriteLine($"Marque = {Marque} Couleur={Couleur} Plaque={PlaqueIma} Vitesse={Vitesse} CompteurKm={CompteurKM}");
            if (Proprietaire != null)
            {
                Proprietaire.Afficher();
            }
        }

        // Méthode de classe
        public static void TestMethodeDeClasse()
        {
            // vitesse = 0;  // Dans une méthode de classe on n'a pas accès à une variable d'instance
            Console.WriteLine($"Méthode de classe, nb voiture:{CompteurVoiture}");  // On peut accéder dans une méthode de classe à une variable de classe
        }

        public static bool CompareVitesse(Voiture va, Voiture vb)
        {
            return va.Vitesse == vb.Vitesse;    // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }

        // Redéfinition des Méthodes d'object
        // ToString => retourne une chaîne qui représente l'objet
        public override string ToString()
        {
            return string.Format($"Voiture[ Marque = {Marque} Couleur={Couleur} Plaque={PlaqueIma} Vitesse={Vitesse} CompteurKm={CompteurKM}]");
        }

        // Equals => permet de tester si l'objet est égal à l'objet passer en paramètre
        // Si on redéfinie Equals, il faut aussi redéfinir GetHashCode
        public override bool Equals(object obj)
        {
            return obj is Voiture voiture &&
                   PlaqueIma == voiture.PlaqueIma;
        }

        public override int GetHashCode()
        {
            return 711479286 + EqualityComparer<string>.Default.GetHashCode(PlaqueIma);
        }
    }

}

