﻿using System;

namespace _05_poo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Appel d'une méthode de classe
            Voiture.TestMethodeDeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"CompteurVoiture{Voiture.CompteurVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture("Ford", "noir", "fr59-fe12");

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Couleur);

            // Accès à une propriété en ecriture (set)
            v1.Marque = "Ford";
            // v1.Vitesse = 10; // On ne peut pas accéder à la propriété vitesse => pas de set public

            Console.WriteLine($"CompteurVoiture{Voiture.CompteurVoiture}");

            // Appel d’une méthode d’instance
            v1.Accelerer(10);
            v1.Afficher();
            v1.Freiner(5);
            v1.Afficher();
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            v1.Afficher();
            Console.WriteLine(v1.EstArreter());

            Voiture v2 = new Voiture("Fiat", "Jaune", "fr59-f234");
            Console.WriteLine($"CompteurVoiture{Voiture.CompteurVoiture}");
            Console.WriteLine($"{v2.Marque} {v2.Vitesse}");

            // Test de l'appel du destructeur
            v2 = null;      // En affectant, les références v2 à null. Il n'y a plus de référence sur l'objet voiture   
                            // Il sera détruit lors du prochain appel du destructeur
            GC.Collect();   // Forcer l'appel le garbage collector

            Voiture v3 = new Voiture { Marque = "Toyota" }; // Initialiseur
            Console.WriteLine($"CompteurVoiture{Voiture.CompteurVoiture}");
            v3.Afficher();
            Console.WriteLine(Voiture.CompareVitesse(v1, v3));

            // Agrégation
            Adresse adr = new Adresse("1, rue Esquermoise", "Lille", "59000");
            Personne per1 = new Personne("John", "Doe", adr);
            v3.Proprietaire = per1;
            v3.Afficher();

            // Exercice Point 
            Point p1 = new Point(1, 2);
            //p1.X = 1;
            //p1.Y = 2;
            p1.Afficher();
            p1.Deplacer(3, 2);
            p1.Afficher();
            Console.WriteLine(p1.Norme());
            Point p2 = new Point(4, 4);
            Console.WriteLine(Point.Distance(p1, p2));

            // Exercice Compte Bancaire
            CompteBancaire cb = new CompteBancaire(300.0, per1);
            //cb.Solde = 300.0;
            //cb.Iban = "fr590000A1";
            // cb.Titulaire = "John Doe";
            cb.Afficher();
            cb.Crediter(200.0);
            cb.Afficher();
            cb.Debiter(600.0);
            cb.Afficher();
            Console.WriteLine(cb.EstPositif());
            Personne per2 = new Personne("Jane", "Doe", adr);
            CompteBancaire cb2 = new CompteBancaire(per2);
            cb2.Afficher();

            // Exercice Agrégation => classe cercle
            Cercle c1 = new Cercle(new Point(0, 0), 2.0);
            Cercle c2 = new Cercle(new Point(3, 0), 2.0);
            Console.WriteLine(Cercle.Colision(c1, c2));
            Point p3 = new Point(1, 1);
            Console.WriteLine(Cercle.Contenu(c1, p3));

            //Indexeur
            Tableau tab = new Tableau(4, 2);
            tab.Add(5);

            Console.WriteLine(tab.Lenght);
            Console.WriteLine(tab[0]);
            tab[1] = 10;

            // Exercice Indexeur
            Point3D pt3 = new Point3D(1, 4, 6);
            pt3.Afficher();
            pt3.X = 23;
            pt3[2] = 45;
            pt3.Afficher();

            // Classe Imbriquée
            // Si la classe imbriquée est public ou internal, on peut l'instancier en dehors du conteneur
            // Container.Element elm = new Container.Element(); 
            // elm.TestElementVarClasse();
            // elm.TestElementVarInstance(c);
            Conteneur c = new Conteneur();
            c.TestConteneur();

            // Classe Partielle => classe définit sur plusieurs fichiers
            Form1 fo1 = new Form1(12);
            fo1.Afficher();

            // Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire("Honda", "Blanc", "fr62-Ab45", true);
            Console.WriteLine(vp1.Gyro);
            vp1.Afficher();
            vp1.Accelerer(20);
            vp1.Afficher();

            // Exercice héritage
            CompteEpargne ce = new CompteEpargne(0.5, per1);
            ce.Crediter(1000.0);
            ce.Afficher();
            ce.CalculInterets();
            ce.Afficher();

            // Object
            // ToString
            Console.WriteLine(v1);
            Console.WriteLine(vp1);

            // Equals
            Voiture ve1 = new Voiture("Lancia", "Rouge", "fr59-AZ45");
            Voiture ve2 = new Voiture("Lancia", "Rouge", "fr59-AZ45");
            Console.WriteLine(ve1 == ve2);
            //Console.WriteLine(ve1.Equals(ve2)); // par défaut Equals dans Object est une comparaison de référence
            Console.WriteLine(ve1.Equals(ve2));

            Type type = v1.GetType();   // retourne le Type de l'instance
            Console.WriteLine(type.Name);
            Console.WriteLine(type.FullName);

            Type type2 = typeof(Voiture); // typeof => retourne le type à partir du nom de la classe   
            Console.WriteLine(type2.Name);
            Console.ReadKey();
        }
    }
}
