﻿using System;

namespace _05_poo
{
    // Classe CompteBancaire

    //  Créer une  Classe CompteBancaire

    // Variables d'instances
    // - un solde  par défaut le solde est à 50.0
    // - un iban
    // - un titulaire
    // Méthodes d'instances
    //  Ajouter les méthodes :
    //  - Afficher : qui affiche le compte sous la forme
    //    ____________________________________________
    //    Solde =  
    //    Iban =  
    //    Titulaire=
    //    _____________________________________________
    //
    //  - Crediter: qui ajoute au compte la valeur passé en paramètre
    //  - Debiter: qui retire au compte la valeur passé en paramètre
    //  - estPositif: qui retourne vrai, si le solde est positif
    //
    // Constructeur
    //  Ajouter: 
    //  - un constructeur par défaut
    //  - un constructeur qui initialise le titulaire
    //  - un constructeur qui initialise le titulaire et le solde
    // Variable de classe
    //  - On ajoute une variable de classe qui permet de compter le nombre de compte  créer 
    //  - On utilise cette variable pour initialiser l'iban en concaténant "fr-5962-0000-"  avec elle
    //
    // Agrégation
    //  - Remplacer la chaine de caractères  titulaire par un type Personne

    class CompteBancaire
    {
        public double Solde { get; protected set; } = 50.0;
        public string Iban { get; private set; }
        public Personne Titulaire { get; set; }

        private static int cptCompte;
        public CompteBancaire()
        {
            cptCompte++;
            Iban = string.Format($"fr - 5962 - 0000 - {(cptCompte.ToString()).PadLeft(3, '0')}");
        }

        public CompteBancaire(Personne titulaire) : this()
        {
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Afficher()
        {
            Console.WriteLine("________________________");
            Console.WriteLine("Solde= {0}", Solde);
            Console.WriteLine("Iban= {0}", Iban);
            if (Titulaire != null)
            {
                Console.WriteLine("Titulaire= ");
                Titulaire.Afficher();
            }
            Console.WriteLine("________________________");
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde += valeur;
            }
        }
        public void Debiter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde > 0;
        }
    }
}
