﻿using System;

namespace _05_poo
{
    // Excercice Classe Point
    //  Créer une Classe Point
    //
    // Variables d'instances
    // - Les coordonnées entière x et y du point
    //
    // Méthodes d'instances
    // Ajouter les méthodes :
    //  - Afficher : qui affiche le point sous la forme (x, y)
    //  - Deplacer :  elle a pour paramètre tx, ty les valeurs que l'on va ajouter au coordonnée du point
    //  - Norme : qui retourne la distance entre le point et l'origine du repère (0,0) : Math.Sqrt(Math.Pow(x,2) + Math.Pow(x,2))
    //
    // Constructeurs
    //  - un constructeur par defaut(sans paramètre)
    //  - un constructeur qui initialise les coordonées du point __x__ et __y__
    //
    // Méthode de classe
    //  - Ajouter un méthode de classe __Distance__ qui permet de calculer la distance entre 2 points
    //   Distance entre 2 points : Math.Sqrt(Math.Pow((x2-x1), 2)+Math.Pow((y2-y1), 2))
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void Afficher()
        {
            Console.WriteLine($"({X},{Y}");
        }

        public void Deplacer(int tx, int ty)
        {
            X += tx;
            Y += ty;
        }

        public double Norme()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }

        public static double Distance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
        }
    }
}
