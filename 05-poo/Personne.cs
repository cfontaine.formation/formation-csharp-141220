﻿using System;

namespace _05_poo
{
    class Personne
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public Adresse Adresse { get; set; }

        public Personne(string prenom, string nom, Adresse adresse)
        {
            Prenom = prenom;
            Nom = nom;
            Adresse = adresse;
        }

        public void Afficher()
        {
            Console.WriteLine($"Prénom={Prenom}, Nom={Nom}");
            Adresse.Afficher();
        }
    }
}
