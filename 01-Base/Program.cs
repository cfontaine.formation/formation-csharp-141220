﻿using System;

namespace _01_Base
{
    // Une enumération est un ensemble de constante
    struct Point
    {
        public int X;
        public int Y;
    }
    // Une enumération est un ensemble de constante
    enum Direction : short { NORD = 90, EST = 0, SUD = 270, OUEST = 180 };
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    //enum Direction : short { NORD = 90, EST = 0, SUD = 270, OUEST = 180 }

    class Program
    {
        // Énumération comme indicateurs binaires
        // Pour éviter toute ambiguïté, il faut assigner explicitement tous les membres
        [Flags]
        enum JourSemaine
        {
            LUNDI = 1,
            MARDI = 2,
            MERCREDI = 4,
            JEUDI = 8,
            VENDREDI = 16,
            SAMEDI = 32,
            DIMANCHE = 64,
            WEEKEND = SAMEDI | DIMANCHE  // On peut spécifier des  combinaisons de membres pendant la déclaration
        }
        static void Main()
        {
            #region Type
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable 
            double hauteur, largeur;
            hauteur = 12.7;
            largeur = 50.0;
            Console.WriteLine(hauteur + " " + largeur); // + => concaténation

            // Déclaration et initialisation de variable    type nomVariable=valeur;
            int j = 42;
            Console.WriteLine(j);

            // Littéral caractère
            char ch = 'd';
            char chutf8 = '\u0045';
            char chHexUtf8 = '\x45';
            Console.WriteLine(ch + " " + chutf8 + " " + chHexUtf8);

            // Littéral booléen
            bool tst = false; // ou true
            Console.WriteLine(tst);

            // Littéral entier -> int par défaut
            long l1 = 1234L;    // L -> Littéral long
            uint ui = 1234U;    // U -> Litéral unsigned
            Console.WriteLine(l1 + " " + ui);

            // Littéral nombre à virgule flottante -> double par défaut
            float f = 123.5F;     // F -> Littéral float
            decimal dm = 123.6M;  // M -> Littéral decimal
            Console.WriteLine(f + " " + dm);


            // Littéral entier -> chagement de base
            int dec = 42;           // décimal (base 10) par défaut
            int hex = 0xFFA1;       // 0x -> héxadecimal 
            int bin = 0b100100;     // 0b -> binaire
            Console.WriteLine(dec + " " + hex + " " + bin);

            // Séparateur _
            double sep = 1_000_000.500;
            // double sd = _100_._00_;  // pas de séparateur en début, en fin , avant et après la virgule 
            Console.WriteLine(sep);

            // Litteral réel
            double d2 = 100.5;
            double d2exp = 3.5e3;
            Console.WriteLine(d2 + " " + d2exp);

            // Type implicite -> var
            var v1 = 10.0;  // v1 -> double
            var v2 = 'a';   // v2 -> char
            Console.WriteLine(v1 + " " + v2);
            #endregion

            #region Conversion
            // Transtypage implicite ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 34;
            double tdi = tii;
            Console.WriteLine(tii + " " + tdi);
            long tli = tii;
            Console.WriteLine(tii + " " + tli);

            // Perte de precision long -> float
            long lp1 = 123456789L;
            long lp2 = 123456788L;
            float fp1 = lp1;
            float fp2 = lp2;
            long lpDiff = lp1 - lp2;
            float fpDiff = fp1 - fp2;
            Console.WriteLine(lpDiff + "  " + fpDiff);

            // Transtypage explicite: cast = (nouveauType)
            double ted = 12.5;
            int tid = (int)ted;
            Console.WriteLine(ted + " " + tid);

            // Dépassement de capacité
            short sh1 = 300;            // 00000001 00101100    300
            sbyte sb1 = (sbyte)sh1;     //          00101100    44
            Console.WriteLine(sh1 + " " + sb1);

            // Checked / Unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            int che = int.MaxValue;     // int.MaxValue => valeur maximal que peut contenir un entier
            //checked
            //{
            //    Console.WriteLine(che + 1);  // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}
            unchecked // (Par défaut)
            {
                Console.WriteLine(che + 1); // plus de vérification de dépassement de capacité
            }

            // Fonction de Convertion
            // La classe Convert contient des méthodes statiques permettant de convertir  un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérque
            int ii = 42;
            double cnv1 = Convert.ToDouble(ii);  // Convertion d'un entier en double
            Console.WriteLine(cnv1);

            int cnv2 = Convert.ToInt32("123");    // Convertion d'une chaine de caractère  en entier
            Console.WriteLine(cnv2);

            // Conversion d'une chaine de caractères en entier
            Console.WriteLine(Int32.Parse("123456"));
            // Console.WriteLine(Int32.Parse("123456a"));   //  Erreur => génère une exception
            Console.WriteLine(Int32.TryParse("123456", out ii));
            // Console.WriteLine(Int32.TryParse("123456a", out ii)); // Erreur => retourn false
            Console.WriteLine(ii);
            #endregion

            // Type référence
            string str1 = "Hello";
            string str2 = null;
            Console.WriteLine(str1 + " " + str2);
            str2 = str1;
            Console.WriteLine(str1 + " " + str2);

            // Nullable : permet d'utiliser une valeur null avec un type valeur
            double? nd = null;
            Console.WriteLine(nd.HasValue);  // La propriété HasValue retourne true si nd contient une valeur (!= null) 
            nd = 12.5;
            Console.WriteLine(nd.HasValue);

            Console.WriteLine(nd.Value);    // Pour récupérer la valeur on peut utiliser la propriété Value
            Console.WriteLine((double)nd);  // ou faire un cast 

            // Constante
            const double PI = 3.14;
            // PI = 1.0;      // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(PI);

            #region Opérateur
            // Opérateur arithméthique
            int op1 = 123;
            int op2 = 67;
            int res = op1 + op2;
            Console.WriteLine(res);
            Console.WriteLine(3 % 2);    //% => modulo(reste de division entière)

            // Opérateur d'incrémentation
            // pré-incrémentation
            int inc = 0;
            res = ++inc; // inc =1 res=1
            Console.WriteLine(inc + " " + res);

            // post-incrémentation
            inc = 0;
            res = inc++; // inc =1 res=0
            Console.WriteLine(inc + " " + res);

            // Affectation combinée
            int ac = 23;
            ac += 11; // ac=ac+11
            Console.WriteLine(ac);

            // Opérateur de comparaison
            bool comp = ac == 34;   // Une comparaison a pour résultat un booléen
            Console.WriteLine(comp);
            comp = ac > 100;
            Console.WriteLine(comp);
            int test = 20;

            // Opérateur court-circuit && et || 
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool compEt = test > 100 && test < 50;    // comme test > 100 est faux,  test < 5O n'est pas évalué
            bool compOu = test == 20 || test > 100;   // comme test == 20 est vrai,  test > 100 n'est pas évalué
            Console.WriteLine(compEt + " " + compOu);

            // Opérateur Binaires (bit à bit)
            byte bin1 = 0b1100110;
            Console.WriteLine(~bin1);       // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(bin1 & 11);   //           et => 10
            Console.WriteLine(bin1 | 1011); //           ou => 1101111
            Console.WriteLine(bin1 ^ 1011); //           ou exclusif => 1101101

            Console.WriteLine(bin1 << 1);   // Décalage à gauche de 1 11001100
            Console.WriteLine(bin1 >> 1);   // Décalage à droite de 1 00110011
            Console.WriteLine(bin1 << 4);   // Décalage à gauche de 4 1100110000

            //  L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null ;
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string fstr = null;
            string rstr = fstr ?? "Bonjour";
            Console.WriteLine(rstr);
            fstr = "Hello";
            rstr = fstr ?? "Bonjour";
            Console.WriteLine(rstr);
            #endregion

            //  Format pour les chaines de caractères
            int xi = 0;
            int yi = 3;
            string resFormat = string.Format("xi={0}, yi={1}", xi, yi);
            Console.WriteLine(resFormat);
            Console.WriteLine("xi={0}, yi={1}", xi, yi);   // on peut définir directement le format dans la mèthode WriteLine 
            Console.WriteLine($"xi={xi},\n yi={yi}");

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers)  
            Console.WriteLine(@"c:\temp\newfile.txt");

            // Promotion numérique
            sbyte pnB = 12;
            int pnRes = -pnB;   // avec un opérateur unaire, un byte est promu en int
            Console.WriteLine($"{pnB}  {pnRes}");

            double pnD = 123.5; // pnI est promu en double => Le type le + petit est promu vers le + grand type des deux 
            int pnI = 12;
            double pnRes2 = pnD + pnI;
            Console.WriteLine(pnRes2);

            // Enumération
            // dir est une variable qui ne pourra accepter que les valeurs de l'enum Direction
            Direction dir = Direction.NORD;
            Console.WriteLine((int)dir);
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            Console.WriteLine(dir.ToString());

            // entier -> enum (cast)
            dir = (Direction)180;
            Console.WriteLine(dir.ToString());

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            dir = (Direction)Enum.Parse(typeof(Direction), "SUD");
            Console.WriteLine(dir.ToString());

            // Énumération comme indicateurs binaires
            JourSemaine jours = JourSemaine.LUNDI | JourSemaine.MERCREDI;

            Console.WriteLine((jours & JourSemaine.MERCREDI) != 0); // teste la présence de MERCREDI => true
            Console.WriteLine((jours & JourSemaine.WEEKEND) != 0);  // teste la présence de WEEKEND => false

            // Structures
            Point p1;
            p1.X = 2;
            p1.Y = 4;
            Console.WriteLine($"X={p1.X} Y={p1.Y}");
            Point p2 = p1;
            if (p1.X == p2.X && p1.Y == p2.Y)
            {
                Console.WriteLine("Les Points sont égaux");
            }
            Console.ReadKey();
        }
    }
}
