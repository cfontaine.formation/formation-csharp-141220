﻿using System;

namespace _04_methodes
{

    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            Console.WriteLine(Somme(2.5, 6.0));

            // Appel de methode (sans retour)
            Afficher(23);

            // Exercice maximum
            int v1 = Convert.ToInt32(Console.ReadLine());
            int v2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Maximum={Maximum(v1, v2)}");

            // Exercice parité
            Console.WriteLine(Even(2));
            Console.WriteLine(Even(3));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int i = 23;
            TestParamValeur(i);
            Console.WriteLine(i);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamRef(ref i);
            Console.WriteLine(i);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // Il faut utiliser le mot clé out pour la définition de méthode mais aussi lors de l'appel
            TestParamOut(out i);
            Console.WriteLine(i);

            // On peut déclarer la variable de retour dans les arguments pendant l'appel de la fonction
            TestParamOut(out int valRetour);
            Console.WriteLine(valRetour);

            // On peut ignorer un paramètre out en le nommant _ 
            TestParamOut(out _);
            Console.WriteLine(Division(3.6, 7.0, out _));

            // Paramètre optionnel
            TestParamDefault(1);
            TestParamDefault(1, 45.9);
            TestParamDefault(1, 45.9, true);

            // Paramètres nommés
            TestParamDefault(b: true, r: 10, d: 12.6);
            TestParamDefault(r: 2, b: true);

            // Exercice: Tableau
            Menu();

            // Nombre de paramètre variable
            Console.WriteLine(Moyenne(1.0, 3.0, 5.0));
            Console.WriteLine(Moyenne());
            Console.WriteLine(Moyenne(1.0, 3.0, 5.0, 5.0, 10.0, 0, -6.0));

            // Surcharge de méthode
            Console.WriteLine(Multiplication(1, 2));
            Console.WriteLine(Multiplication(1.5, 3.4));
            Console.WriteLine(Multiplication(1.5, 2));

            // Méthode récursive
            Console.WriteLine(Factorial(3));

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (string arg in args)
            {
                Console.WriteLine(arg);
            }

            Console.ReadKey();
        }

        static double Somme(double va, double vb)
        {
            return va + vb; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur (expression à droite)
        }

        static void Afficher(int v) // void => pas de valeur retournée
        {
            Console.WriteLine(v);
            //   avec void => return; ou return peut être omis
        }

        // Exercice maximum
        // Ecrire une fonction Maximum qui prends en paramètre 2 nombres et elle retourn le maximum
        // Saisir 2 nombres et afficher le maximum entre ces 2 nombres
        static int Maximum(int v1, int v2)
        {
            return v1 > v2 ? v1 : v2;

            //if(v1 > v2)
            //{
            //    return v1;
            //}
            //else
            //{
            //    return v2;
            //}
        }

        // Exercice Parités
        // Écrire une fonction __Even__ qui prend un entier en paramètre
        // elle retourne vrai si il est paire
        static bool Even(int valeur)
        {
            return valeur % 2 == 0;

            // return valeur % 2 == 0 ? true : false;

            //if(valeur%2==0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        // Passage par valeur
        static void TestParamValeur(int x)
        {
            x = 100;    // La modification  de la valeur du paramètre x n'a pas d'influence en dehors de la méthode
            Console.WriteLine(x);
        }

        // Passage par référence => ref
        static void TestParamRef(ref int x)
        {
            x = 100;
            Console.WriteLine(x);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int x)
        {
            x = 250;     // La méthode doit obligatoirement affecter une valeur aux paramètres out
        }

        static double Division(double v1, double v2, out bool erreur)
        {
            if (v2 == 0.0)
            {
                erreur = true;
                return 0.0;
            }
            else
            {
                erreur = false;
                return v1 / v2;
            }

        }

        // On peut donner aux arguments passés par valeur des valeurs par défaut
        static void TestParamDefault(int r, double d = 12.5, bool b = false)
        {
            Console.WriteLine($"{r} {d} {b}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(params double[] valeurs)
        {
            double somme = 0.0;
            foreach (var val in valeurs)
            {
                somme += val;
            }
            return somme;
        }

        // Surcharge_de_méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static double Multiplication(double v1, double v2)
        {
            Console.WriteLine("2 Doubles");
            return v1 * v2;
        }

        static int Multiplication(int i1, int i2)
        {
            Console.WriteLine("2 Entiers");
            return i1 * i2;
        }


        static double Multiplication(double d1, int i2)
        {
            Console.WriteLine("1 double et 1 entier");
            return d1 * i2;
        }

        // Récursivite
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }

        // Exercice Tableau

        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui permet de saisir :
        //  - La taille du tableau
        //  - Les éléments du tableau
        // Écrire une méthode qui calcule :
        // - le minimum d’un tableau d’entier
        // - le maximum
        // - la moyenne
        // Faire un menu qui permet de lancer ces méthodes

        static void AfficherTableau(int[] tab)
        {
            Console.Write("[ ");
            foreach (var val in tab)
            {
                Console.Write("{0} ", val);
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTableau()
        {
            Console.WriteLine("Entrer la taile du tableau");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tab = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"Tab[{i}] = ");
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tab;
        }

        static void Calcul(int[] tab, out int minimum, out int maximum, out double moyenne)
        {
            minimum = int.MaxValue;
            maximum = int.MinValue;
            double somme = 0.0;
            foreach (var elm in tab)
            {
                if (elm > maximum)
                {
                    maximum = elm;
                }
                if (elm < minimum)
                {
                    minimum = elm;
                }
                somme += elm;
            }
            moyenne = somme / tab.Length;
        }

        static void AfficheMenu()
        {
            Console.WriteLine("1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le minimum, le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            int[] tab = null;
            int choix;
            do
            {
                AfficheMenu();
                choix = Convert.ToInt32(Console.ReadLine());
                if ((choix == 2 || choix == 3) && tab == null)
                {
                    Console.WriteLine("Le tableau n'est pas saisie");
                }
                else
                {
                    switch (choix)
                    {
                        case 1:
                            tab = SaisirTableau();
                            break;
                        case 2:
                            AfficherTableau(tab);
                            break;
                        case 3:
                            Calcul(tab, out int min, out int max, out double moyenne);
                            Console.WriteLine($"Minimum:{min} Maximum:{max} Moyenne:{moyenne}");
                            break;
                    }
                }
                Console.WriteLine();
            }
            while (choix != 0);
        }
    }
}
