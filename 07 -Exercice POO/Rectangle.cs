﻿namespace _07__Exercice_POO
{
    class Rectangle : Forme
    {
        public double Largeur { get; set; }
        public double Hauteur { get; set; }
        public Rectangle(double largeur, double hauteur, Couleurs couleur) : base(couleur)
        {
            Largeur = largeur;
            Hauteur = hauteur;
        }

        public override double CalculSurface()
        {
            return Largeur * Hauteur;
        }

        public override string ToString()
        {
            return string.Format($"Rectangle [ Largeur={Largeur} Hauteur={Hauteur} {base.ToString()}]");
        }
    }
}
