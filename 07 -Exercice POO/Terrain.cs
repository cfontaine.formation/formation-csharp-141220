﻿namespace _07__Exercice_POO
{
    class Terrain
    {
        Forme[] tabForme = new Forme[10];
        int nbForme;

        public Terrain()
        {
        }

        public void Add(Forme forme)
        {
            if (nbForme < tabForme.Length)
            {
                tabForme[nbForme] = forme;
                nbForme++;
            }
        }

        public double SurfaceTotal()
        {
            double surface = 0.0;
            for (int i = 0; i < nbForme; i++)
            {
                surface += tabForme[i].CalculSurface();
            }
            return surface;
        }

        public double SurfaceCouleur(Couleurs couleur)
        {
            double surface = 0.0;
            for (int i = 0; i < nbForme; i++)
            {
                if (tabForme[i].Couleur == couleur)
                {
                    surface += tabForme[i].CalculSurface();
                }
            }
            return surface;
        }
    }
}
