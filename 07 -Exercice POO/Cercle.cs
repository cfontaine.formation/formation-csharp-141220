﻿using System;

namespace _07__Exercice_POO
{
    class Cercle : Forme
    {
        public double Rayon { get; set; }
        public Cercle(Couleurs couleur, double rayon) : base(couleur)
        {
            Rayon = rayon;
        }
        public override double CalculSurface()
        {
            return Rayon * Rayon * Math.PI;
        }

        public override string ToString()
        {
            return string.Format($"Cercle [ Rayon={Rayon} { base.ToString()}");
        }
    }
}
