﻿using System;

namespace _06___Polymorphisme
{
    class Canard : Animal, IPeutMarcher, IPeutVoler     // On peut implémenter plusieurs d'interface
    {
        public Canard(int age, double poid) : base(age, poid)
        {
        }

        public void Atterir()
        {
            Console.WriteLine("Le canard atterit");
        }

        public void Courir()
        {
            Console.WriteLine("Le canard court");
        }

        public void Decoler()
        {
            Console.WriteLine("Le canard décolle");
        }

        public override void EmmetreUnSon()
        {
            Console.WriteLine("Coin Coin");
        }

        public void Marcher()
        {
            Console.WriteLine("Le canard marche");
        }
    }
}
