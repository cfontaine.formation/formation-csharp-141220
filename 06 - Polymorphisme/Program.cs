﻿using System;

namespace _06___Polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animal a1 = new Animal(10, 5000);     // Impossible la classe est abstraite
            //a1.EmmetreUnSon();

            Chien c1 = new Chien("Laika", 2, 3500);
            c1.EmmetreUnSon();

            Animal c2 = new Chien("Idefix", 10, 2000);  // On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                        // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
            c2.EmmetreUnSon();                          // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée


            if (c2 is Chien)         // test si c2 est de "type" Chien
            {
                //  Chien c2c = (Chien) c2;  // Pour passer d'une super-classe à une sous-classe, il faut le faire expicitement avec un cast ou avec l'opérateur as
                Chien c2c = c2 as Chien;     // as equivalant à un cast pour un objet
                c2c.EmmetreUnSon();          // avec la référence c2c (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
                Console.WriteLine(c2c.Nom);
            }

            Animal a2 = new Chat(3, 3000, 9, "Garfield");
            a2.EmmetreUnSon();

            if (a2 is Chien)
            {
                // Chien chien2 = (Chien)a2;
                Chien chien2 = a2 as Chien;
                chien2.EmmetreUnSon();
            }

            if (a2 is Chat chat1)      // Avec l'opérateur is on peut tester si a2 est de type Chat et faire la conversion en meme temps 
            {
                chat1.EmmetreUnSon();
            }

            Animal[] tab = new Animal[4];
            tab[0] = new Chien("Snoopy", 15, 4000);
            tab[1] = new Chat(15, 4000, 7, "Garfield");
            tab[2] = new Chien("Idefix", 15, 4000);
            tab[3] = new Canard(3, 4000);
            foreach (Animal a in tab)
            {
                a.EmmetreUnSon();
            }

            IPeutMarcher ip1 = new Chien("Tom", 12, 5000);  // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            ip1.Marcher();                                  // L'objet chien n'est vu que comme un interface IPeutMarcher, on ne peut utiliser que les méthode de IPeutMarcher (marcher, courrir)
            Console.ReadKey();
        }
    }
}
