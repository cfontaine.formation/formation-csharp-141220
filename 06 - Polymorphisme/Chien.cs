﻿using System;

namespace _06___Polymorphisme
{
    class Chien : Animal, IPeutMarcher       // La classe Chien hérite de la classe Animal et implémente l'interface IPeutMarcher
    {
        public string Nom { get; set; }
        public Chien(string nom, int age, double poid) : base(age, poid)
        {
            Nom = nom;
        }

        public sealed override void EmmetreUnSon()      // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal 
        {                                               // sealed sur une méthode interdit la redéfiniton de la méthode dans les sous-classes
            Console.WriteLine($"{Nom} aboie");
        }

        public void Marcher()   // Impléméntation de la méthode Marcher de l'interface IPeutMarcher
        {
            Console.WriteLine($"{Nom} marche");
        }

        public void Courir()    // Impléméntation de la méthode Courrir de l'interface  IPeutMarcher
        {
            Console.WriteLine($"{Nom} court");
        }
    }
}
