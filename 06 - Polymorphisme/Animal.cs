﻿namespace _06___Polymorphisme
{
    abstract class Animal   // Animal est une classe abstraite, elle ne peut pas être instantiée elle même
    {                       // mais uniquement par l'intermédiaire de ses classses filles
        public int Age { get; set; }
        public double Poid { get; set; }

        public Animal(int age, double poid)
        {
            Age = age;
            Poid = poid;
        }

        // public virtual void EmetreUnSon()
        // {
        //    Console.WriteLine("L'animal émet un son");
        // }
        public abstract void EmmetreUnSon();     // Méthode abstraite qui doit obligatoirement redéfinit par les classes filles
    }
}
