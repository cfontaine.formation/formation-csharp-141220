﻿using System;

namespace _06___Polymorphisme
{
    class Chat : Animal, IPeutMarcher
    {
        public string Nom { get; set; }
        public int NbVie { get; set; } = 9;
        public Chat(int age, double poid, int nbVie, string nom) : base(age, poid)
        {
            NbVie = nbVie;
            Nom = nom;
        }

        public override void EmmetreUnSon()
        {
            Console.WriteLine($"{Nom} miaule");
        }

        public void Marcher()
        {
            Console.WriteLine($"{Nom} marche");
        }

        public void Courir()
        {
            Console.WriteLine($"{Nom} court");
        }
    }
}
