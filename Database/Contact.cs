﻿using System;

namespace Database
{
    public class Contact : DbObject
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public DateTime JourNaissance { get; set; }

        public string Email { get; set; }

        public Contact(string prenom, string nom, DateTime jourNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            JourNaissance = jourNaissance;
            Email = email;
        }

        public override string ToString()
        {
            return string.Format($"Contact[ Id: {base.ToString()} Prénom: {Prenom} Nom: {Nom} Date de naissance: {JourNaissance} Email: {Email}]");
        }
    }
}
