﻿namespace Database
{
    // La classe abstract DbObject est la classe mère des objets persistés dans la base de données
    public abstract class DbObject
    {
        public long Id { get; set; }

        public override bool Equals(object obj)
        {
            return obj is DbObject @object &&
                   Id == @object.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format($"{Id}");
        }
    }
}
