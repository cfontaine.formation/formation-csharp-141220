﻿using System;

namespace _12_ClasseBase
{
    class TestGeneric<T>
    {
        T a;
        T b;

        public TestGeneric(T a, T b)
        {
            this.a = a;
            this.b = b;
        }

        public void Afficher()
        {
            Console.WriteLine(a + " " + b);
        }

        public void TestMethodeGeneric<U>(U a)
        {
            Console.WriteLine(a);
        }
    }

}
