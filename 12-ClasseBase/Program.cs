﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace _12_ClasseBase
{
    class Program
    {
        static void Main(string[] args)
        {
            // Chaine de caractère
            string str = "HellO World";
            string str2 = "Bonjour";
            string str3 = new string('a', 10);
            Console.WriteLine(str.Length);   // Length -> Nombre de caractère de la chaine de caractère

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str.CompareTo(str2));
            Console.WriteLine(string.Compare(str2, str));

            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            Console.WriteLine("Bonjour".Equals(str2));
            Console.WriteLine("Bonjour" == str2);

            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            Console.WriteLine(string.Concat(str, "    ", "!!!!!"));
            // La méthode Join concaéènent les chaines,  en les séparants par un caractère (ou ne chaine) de séparation
            string csv = string.Join(";", str, "    ", "!!!!!");

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            Console.WriteLine(csv);
            string[] tab = csv.Split(';');
            foreach (string s in tab)
            {
                Console.WriteLine(s);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine ou pour le nombre de caractère passé en paramètre
            Console.WriteLine(str.Substring(6));
            Console.WriteLine(str.Substring(4, 4));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str.Insert(5, "----"));

            // Les caractères sont supprimés à partir de l'indice pour le nombre de caractère passé en paramètre
            Console.WriteLine(str.Remove(5, 1));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str.StartsWith("Hello"));
            Console.WriteLine(str.StartsWith("Bonjour"));

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str.IndexOf('o'));
            Console.WriteLine(str.IndexOf('o', 5)); // idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str.IndexOf('o', 8)); // retourne -1, si le caractère n'est pas trouvé 

            // Remplace toutes les chaines (ou caratère) oldValue par newValue 
            Console.WriteLine(str.Replace("o", "a"));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str.Contains("Hell"));
            Console.WriteLine(str.Contains("Azerty"));

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str.PadLeft(50, '_'));
            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str.PadRight(50, '_'));

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("    \t \n azerty  uiop  \t \n ".Trim());
            Console.WriteLine("    \t \n azerty  uiop  \t \n ".TrimStart());     // idem uniquement en début de chaine
            Console.WriteLine("    \t \n azerty  uiop  \t \n ".TrimEnd());       // idem uniquement en fin de chaine

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str.ToUpper());

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str.ToLower().Trim().Substring(6));

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder();
            sb.Append("Hello");
            sb.Append(" ");
            sb.Append("World");
            string str4 = sb.ToString();
            string str5 = str4.Trim().ToUpper();
            Console.WriteLine(str5);

            // Exercice Inversion de chaine
            Console.WriteLine(Inverser("Bonjour"));
            Console.WriteLine(Inverser("Radar"));

            // Exercice Inversion de chaine
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Palindrome("Radar"));

            DateTime d = DateTime.Now;  // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);
            DateTime debut2021 = new DateTime(2021, 1, 1);
            Console.WriteLine(debut2021);
            Console.WriteLine(debut2021 - d);

            TimeSpan duree = new TimeSpan(10, 0, 0, 0);    // TimeSpan => représente une durée
            Console.WriteLine(d.Add(duree));
            Console.WriteLine(d.AddDays(12));
            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());
            Console.WriteLine(d.Hour);
            Console.WriteLine(d.DayOfWeek);
            Console.WriteLine(d.Ticks);

            Console.WriteLine(DateTime.Parse("1955/01/23"));
            Console.WriteLine(d.ToString("dd-MMMM-yy"));

            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("AZERTY");
            lst.Add(1);
            lst.Add(false);
            Console.WriteLine(lst.Count);
            Console.WriteLine(lst[0]);
            object obj = lst[0];
            if (obj is string)   // is permet de tester le type de l'objet
            {
                string strConv = obj as string;     // as equivaut à un cast pour les objets
                Console.WriteLine(strConv);
            }


            /// Collection fortement typée => type générique
            List<int> lstFt = new List<int>();
            lstFt.Add(2);
            lstFt.Add(22);
            lstFt.Add(342);
            lstFt.Add(78);
            //lstFt.Add("zeer8");  // On ne peut plus qu'ajouter des entiers => sinon erreur de complilation
            Console.WriteLine(lstFt[1]);    // Accès à un élément de la liste
            foreach (var i in lstFt)    // Parcourir la collection complétement
            {
                Console.WriteLine(i);
            }

            // Dictionary => association clé/valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(123, "Hello");        // Add => ajout d'un valeur associé à une clé
            // m.Add(123, "World");     // On ne peut pas ajouter si la cléexite déjà
            m[123] = "World";           // 
            m.Add(14, "Bonjour");
            m.Add(12, "Décembre");
            Console.WriteLine(m[12]);  // accès à un élément m[clé] => valeur
            m.Remove(12);

            // Parcourir un dictionnary
            foreach (var kv in m) // KeyValuePair<int,string>
            {
                Console.WriteLine($"Key={kv.Key} Value={kv.Value}");
            }

            // Parcourrir un collection avec un Enumérateur
            IEnumerator it = lstFt.GetEnumerator();
            it.Reset();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // Type généric
            TestGeneric<string> c1 = new TestGeneric<string>("azerty", "hello");
            c1.Afficher();
            TestGeneric<int> c2 = new TestGeneric<int>(2, 2);
            c2.Afficher();
            // c2.Test<double>(12.0);
            c2.TestMethodeGeneric(12.0); // Le type est déduit du type du paramètre

            Console.ReadKey();
        }

        // Exercice Inversion de chaine
        // Écrire la fonction Inversee qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // bonjour →  ruojnob
        public static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }

        // Exercice  Palindrome
        // Écrire une méthode __Palindrome__ qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        //    SOS →  true
        //      //    Bonjour →  false
        //      //    radar →  true
        public static bool Palindrome(string str)
        {
            string tmp = str.Trim().ToLower();
            return tmp == Inverser(tmp);
        }
    }


}
