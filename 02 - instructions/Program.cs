﻿using System;

namespace _02___instructions
{
    class Program
    {
        static void Main()
        {
            // Exercice: Salutation
            // Faire un programme qui:
            // - Affiche le message: Entrer votre nom
            // - Permet de saisir le nom
            // -Affiche __Bonjour,__ complété du nom saisie
            Console.WriteLine("Entrer votre nom");
            string nom = Console.ReadLine();
            Console.WriteLine("Bonjour, {0}", nom);

            // Exercice: Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("{0} + {1} = {2}", a, b, a + b);

            // Exercice: Trie de 3 Valeurs
            // Saisir 3 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme: 1.5 < 3.7 < 10.5
            double v1 = Convert.ToDouble(Console.ReadLine());
            double v2 = Convert.ToDouble(Console.ReadLine());
            double v3 = Convert.ToDouble(Console.ReadLine());
            double tmp;
            if (v1 > v2)
            {
                tmp = v1;
                v1 = v2;
                v2 = tmp;
            }
            if (v3 < v1)
            {
                Console.WriteLine("{0}<{1}<{2}", v3, v1, v2);
            }
            else if (v3 > v2)
            {
                Console.WriteLine("{0}<{1}<{2}", v1, v2, v3);
            }
            else
            {
                Console.WriteLine("{0}<{1}<{2}", v1, v3, v2);
            }

            // Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4 (exclus) et 7 (inclus)
            int valeur = Convert.ToInt32(Console.ReadLine());
            if (valeur > -4 && valeur <= 7)
            {
                Console.WriteLine("{0} fait partie de l'interface", valeur);
            }

            // Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // un double v1
            // une chaine de caractère opérateur qui a pour valeur valide: +-* /
            // un double v2

            // Afficher:
            // Le résultat de l’opération
            // Une message d’erreur si l’opérateur est incorrecte
            // Une message d’erreur si l’on fait une division par 0
            double val1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double val2 = Convert.ToDouble(Console.ReadLine());
            switch (op)
            {
                case "+":
                    Console.WriteLine("{0} + {1} = {2}", val1, val2, val1 + val2);
                    break;
                case "-":
                    Console.WriteLine("{0} - {1} = {2}", val1, val2, val1 - val2);
                    break;
                case "*":
                    Console.WriteLine("{0} x {1} = {2}", val1, val2, val1 * val2);
                    break;
                case "/":
                    if (val2 == 0) // val2>-0.000001 && val2<0.000001
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine("{0} / {1} = {2}", val1, val2, val1 / val2);
                    }
                    break;
                default:
                    Console.WriteLine($"Erreur L'opérateur {op} n'est pas valide");
                    break;
            }
            // Exercice (Opérateur ternaire): Valeur absolue 
            // Saisir 1 chiffres et afficher la valeur absolue sous la forme | -1.85 |= 1.85
            double valAbs = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("|{0}| = {1}", valAbs, valAbs > 0.0 ? valAbs : -valAbs);

            // Instructions de branchement
            // break
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
                if (i == 3)   // break => termine la boucle
                {
                    break;
                }
            }

            // continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine(i);
            }

            // goto
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == 3)
                    {
                        goto EXIT;     // Utilisation de goto pour sortir de boucles imbriquées
                    }
                    Console.WriteLine("{0} {1}", i, j);
                }
            }
        EXIT: int jours = 1;
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto case 7;    // Utilisation de goto pour transférer le contrôle à un case ou à l’étiquette par défaut d’une instruction switch    
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Table de multiplication
            // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
            //    1 X 4 = 4
            //    2 X 4 = 8
            //    …
            //    9 x 4 = 36
            // Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
            for (; ; ) // while(true)
            {
                int mul = Convert.ToInt32(Console.ReadLine());
                if (mul < 1 || mul > 9)
                {
                    break;
                }
                for (int i = 1; i < 10; i++)
                {
                    Console.WriteLine($"{mul} x {i} = {mul * i}");
                }
            }

            //Quadrillage
            //Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
            // ex: pour 2 3
            //    [ ][ ]
            //    [ ][ ]
            //    [ ][ ]
            Console.WriteLine("Entrée le nombre de colonne");
            int col = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Entrée le nombre de ligne");
            int row = Convert.ToInt32(Console.ReadLine());
            for (int l = 0; l < row; l++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[ ]");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
