﻿using System;

namespace _03_tableaux
{
    class Program
    {
        static void Main()
        {
            #region Tableau à une dimension
            string[] tabStr = new string[5];    // Déclaration d'un tableau

            // Tableau initialiser pour les entiers, les nombres à virgules flottantes =0
            //                      pour les booléen = false
            //                      pour les caractères = '\u0000'
            //                      pour les référence = null
            Console.WriteLine(tabStr[0] == null);

            tabStr[0] = "hello";
            tabStr[1] = "world";
            tabStr[2] = "Bonjour";
            tabStr[3] = "le";
            tabStr[4] = "monde";
            Console.WriteLine(tabStr[2]);       // Accès au troisième élément du tableau
            Console.WriteLine(tabStr.Length);   // Length => Nombre d'élément du tableau

            // Parcourir un tableau
            for (int i = 0; i < tabStr.Length; i++)
            {
                Console.WriteLine("tabStr[{0}]={1}", i, tabStr[i]);
            }

            // Déclarer un tableau en l'initialisant
            int[] tabInt = { 2, -7, 3, 8, 9 };
            Console.WriteLine(tabInt[3]);
            Console.WriteLine(tabInt.Length);

            // Parcourir complétement un tableau
            foreach (int val in tabInt)     // ou foreach (var val in tabInt)
            {
                Console.WriteLine(val);
            }

            // Exercice Tableau
            // Maximum et Moyenne d'un tableau d'entier 
            // Trouver la valeur maximale d’un tableau de 5 entier: - 7,4,8,0,-3
            // Puis la moyenne
            // Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            // int[] tab = { -7, 4, 8, 0, -3 };

            // Saisie de la taille du tableau
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tab = new int[size];

            //Saisie des éléments du tableau
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }

            int maximum = int.MinValue;
            double somme = 0;
            foreach (var v in tab)
            {
                if (v > maximum)
                {
                    maximum = v;
                }
                somme += v;
            }
            Console.WriteLine($"maximum={maximum} moyenne={somme / tab.Length}");
            #endregion

            #region  Tableau Multidimmension
            double[,,] tab3d = new double[2, 3, 2];     // Déclaration d'un tableau à 3 dimensions
            tab3d[1, 1, 1] = 123.45;                    // Accès à un élémént d'un tableau à 3 dimensions
            Console.WriteLine(tab3d[0, 0, 0]);
            Console.WriteLine(tab3d[1, 1, 1]);
            Console.WriteLine(tab3d.Length);            // Nombre d'élément du tableau

            Console.WriteLine(tab3d.Rank);              // Nombre de dimension du tableau
            for (int i = 0; i < tab3d.Rank; i++)
            {
                Console.WriteLine(tab3d.GetLength(i));  // Nombre d'élément pour la dimension
            }

            // Parcourir complétement un tableau à 3 dimensions
            foreach (var v in tab3d)
            {
                Console.WriteLine(v);
            }
            #endregion

            #region  Tableau de tableau
            int[][] tabEsc = new int[3][];   // Déclaration d'un tableau de tableau
            tabEsc[0] = new int[3];
            tabEsc[1] = new int[2];
            tabEsc[2] = new int[4];
            tabEsc[2][1] = 12;                    // accès à un élément
            Console.WriteLine(tabEsc[2][1]);

            // Parcourir un tableau de tableau
            foreach (int[] lv in tabEsc)
            {
                foreach (int valeur in lv)
                {
                    Console.WriteLine(valeur);
                }
            }
            Console.WriteLine(tabEsc.Length);    // Nombre de Ligne
            for (int i = 0; i < tabEsc.Length; i++)
            {
                Console.WriteLine(tabEsc[i].Length);    // Nombre de colonne pour chaque ligne
            }
            #endregion

            Console.ReadKey();
        }
    }
}
